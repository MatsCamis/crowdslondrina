import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarkerModalPage } from './marker-modal';

@NgModule({
  declarations: [
    MarkerModalPage,
  ],
  imports: [
    IonicPageModule.forChild(MarkerModalPage),
  ],
})
export class MarkerModalPageModule {}
