import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

/**
 * Generated class for the MarkerModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-marker-modal',
  templateUrl: 'marker-modal.html',
})
export class MarkerModalPage {
  lat: string = this.navParams.get('lat');
	lng: string = this.navParams.get('lng');
  markers: AngularFireList<any>;  
  selectValue: any;
  nome:any;
  telefone:any;
  email:any;
  desc: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public afd: AngularFireDatabase, private alertCtrl: AlertController) {
   this.markers = afd.list('/markers');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarkerModalPage');
    console.log(this.lat);
    console.log(this.lng);
  	
  }
  saveData(){
    const newMarker = this.markers.push({});
    newMarker.set({
      id: newMarker.key,
      lat: this.lat,
      lng: this.lng,
      problema: this.selectValue,
      nome: this.nome,
      telefone: this.telefone,
      email: this.email,
      desc: this.desc,
      status: "esperando"
    });
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      subTitle: 'Dados Enviados com sucesso',
      buttons: ['ok']
    });
    alert.present().then( () =>{
      this.viewCtrl.dismiss();
    });
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  selected(value){
    this.selectValue = value;
    console.log("Selected"+this.selectValue);
  }

}
