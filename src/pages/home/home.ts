import { Component, ViewChild } from '@angular/core';
import { ModalController, NavController, AlertController } from 'ionic-angular';
import { MarkerModalPage } from '../marker-modal/marker-modal';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	
	@ViewChild('map') mapElement;  
	map: any;
    infoWindows: any;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public modalCtrl: ModalController, public afd: AngularFireDatabase) {
    this.infoWindows = [];
  }
  drawMarkers(){
    this.afd.list('/markers').valueChanges().subscribe(data => {

        for(let fireobj of data) {
            if (fireobj.status != "esperando") {
                var position = new google.maps.LatLng(fireobj.lat, fireobj.lng);
                var marker = new google.maps.Marker({
                    position: position, 
                    problema: fireobj.problema,
                    title: fireobj.status,
                    animation: google.maps.Animation.DROP,
                    icon: fireobj.status == "pronto"? 'http://maps.google.com/mapfiles/ms/icons/green-dot.png' : 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
                });               
                marker.setMap(this.map);
                this.addInfoWindowToMarker(marker);

            }
      }
  });

  }
 addInfoWindowToMarker(marker) {
  var infoWindowContent = '<div id="content"><label id="firstHeading" class="firstHeading">' + marker.problema + ' ('+ marker.title +') </label></div>';
  var infoWindow = new google.maps.InfoWindow({
    content: infoWindowContent
  });
  marker.addListener('click', () => {
    this.closeAllInfoWindows();
    infoWindow.open(this.map, marker);
  });
  this.infoWindows.push(infoWindow);
}
closeAllInfoWindows() {
  for(let window of this.infoWindows) {
    window.close();
  }
}
  CenterControl(controlDiv, map, styledMapType, alert, modalCtrl, styledMapTypeNormal) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Criar novo Marcador';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = 'Criar novo Marcador';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', (e) => {
          map.mapTypes.set('styled_map', styledMapType);          
          map.setMapTypeId('styled_map');
          var listener1 = map.addListener('click', (e) => {
            placeMarkerAndPanTo(e.latLng, map);
            window.setTimeout(function() {
              openModal(e.latLng.lat(),e.latLng.lng(), listener1);
            }, 1500);
          });

          alert.present();
          controlUI.removeChild(controlText);
          
          controlDiv.removeChild(controlUI);
        });

        function placeMarkerAndPanTo(latLng, map) {
          var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            animation: google.maps.Animation.DROP
          });
          map.panTo(latLng);
          map.setZoom(17);
        }

        function openModal(lat,lng,listener1) {
          let obj = {lat: lat, lng: lng}
          let myModal = modalCtrl.create(MarkerModalPage, obj);

            myModal.onDidDismiss(data => {
              map.setMapTypeId('normal_map');
              controlDiv.appendChild(controlUI);
              controlUI.appendChild(controlText);
              google.maps.event.clearListeners(map, 'click');
            });

          myModal.present();
        }
      } 
      
  ionViewDidLoad(){
    this.initMap();
    this.drawMarkers();
  }

  initMap(){
  	let latLng = new google.maps.LatLng(-23.310902,-51.168072);
var styledMapTypeNormal = new google.maps.StyledMapType(
  [
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
],
            {name: 'normal_map'});

  	let mapOptions = {
  		center: latLng,
  		zoom: 15,
  		styles:[
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
],
      mapTypeControl: false,
      fullscreenControl: false,
      streetViewControl: false
  	};
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.map.mapTypes.set('normal_map', styledMapTypeNormal);
  	
    
    var styledMapType = new google.maps.StyledMapType(
            [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    }
],
            {name: 'Styled Map'});

  let alert = this.alertCtrl.create({
    title: 'Novo Marcador',
    subTitle: 'Clique no mapa para criar uma nova marcação',
    buttons: ['Entendi']
  });

  var centerControlDiv = document.createElement('div');

  var centerControl = new this.CenterControl(centerControlDiv, this.map, styledMapType, alert, this.modalCtrl, styledMapTypeNormal);

  this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);

  }


}
