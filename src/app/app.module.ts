import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MarkerModalPage } from '../pages/marker-modal/marker-modal';
import { FirebaseServiceProvider } from '../providers/firebase-service/firebase-service';

import { HttpModule } from '@angular/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';


var firebaseConfig = {
    apiKey: "AIzaSyARW_0TZyl1ktOSCli9FREKypk04kRhVHg",
    authDomain: "londrina-a9c93.firebaseapp.com",
    databaseURL: "https://londrina-a9c93.firebaseio.com",
    projectId: "londrina-a9c93",
    storageBucket: "londrina-a9c93.appspot.com",
    messagingSenderId: "254011673807"
  };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MarkerModalPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MarkerModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseServiceProvider
  ]
})
export class AppModule {}
